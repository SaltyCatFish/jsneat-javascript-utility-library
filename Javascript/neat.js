/**
 * Neat Module
 * @module neat
 */

/*global module*/

/** Creates a neat object */
module.exports = (function () {
    "use strict";
    var
        nato,
        isLetter,
	isNumber;

    /** Takes a string of characters and returns its nato alphabet equivalent */
    nato = (function () {
        var letters = {
            "A": "Alpha",
            "B": "Bravo",
            "C": "Charlie",
            "D": "Delta",
            "E": "Echo",
            "F": "Foxtrot",
            "G": "Golf",
            "H": "Hotel",
            "I": "India",
            "J": "Juliett",
            "K": "Kilo",
            "L": "Lima",
            "M": "Mike",
            "N": "November",
            "O": "Oscar",
            "P": "Papa",
            "Q": "Quebec",
            "R": "Romeo",
            "S": "Sierra",
            "T": "Tango",
            "U": "Uniform",
            "V": "Victor",
            "W": "Whiskey",
            "X": "X-ray",
            "Y": "Yankee",
            "Z": "Zulu"
        };

        return function (word) {
            return word.split("").map(function (v) {
                if (isLetter(v)) {
                    return letters[v.toUpperCase()];
                } else {
                    return v;
                }
            }).join(" ");
        };
    }());

    /** Check whether v is a valid letter */
    isLetter = function (v) {
        return v.length === 1 && v.match(/[a-z]/i);
    };
    
    /** Check whether n is a valid number */
    isNumber = function (n) {
	return !isNaN(n)
    };

    //Export functions as object
    return {
        isLetter: isLetter,
	isNumber: isNumber,
        nato: nato
    };
}());

# README #

To use, include the following line at the top of your Javascript file:

```
#!javascript

var neat = require("./neat");
```

This library uses a module factory pattern, so you don't have to worry about naming conflicts with other exported modules.


### What is this repository for? ###

* A utility library to make some common JS tasks neat.
* Version 1.0

### Available Functions ###

|Function name  |Description                                    |
|---------------|-----------------------------------------------|
|`nato()`       |Returns the nato alphabet translation of a word|
|`isNumber()`   |Determines if a variable is a number           |
|`isLetter()`   |Determines if a variable is a letter           |


* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)